module.exports = {
    globals: {
        'ts-jest': {
            tsConfig: './test/tsconfig.json'
        }
    },
    preset: 'ts-jest',
    testEnvironment: 'node',
    setupFiles: [
        './test/load-env.ts'
    ]
}