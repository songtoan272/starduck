import {ICoffee} from "./definitions/models/coffee.interface";

export class Coffee implements ICoffee {
    readonly intensity: number;
    readonly name: string;
    readonly price: number;

    constructor(coffee: ICoffee) {
        this.intensity = coffee.intensity;
        this.name = coffee.name;
        this.price = coffee.price;
    }
}