import {IsNumber, IsString, Max, Min, MinLength} from "class-validator";
import {ICoffee} from "../models";

export class CoffeeDTO implements ICoffee {

    @IsString()
    @MinLength(1)
    readonly name!: string;

    @IsNumber()
    @Min(0)
    readonly price!: number;

    @IsNumber()
    @Min(0)
    @Max(100)
    readonly intensity!: number;
}