import {ICoffee} from "../models";
import {IBaseDAO} from "./base.dao.interface";

// Data Access Object
export interface ICoffeeDAO extends IBaseDAO<ICoffee> {
    save(coffee: ICoffee): Promise<ICoffee>;
    searchByName(name: string): Promise<ICoffee[]>
}