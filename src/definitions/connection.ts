export interface Connection {
    close(callback: (err?: Error) => void): any;
}

export async function closeConnections(connections: Connection[]): Promise<void> {
    const promises = connections.map((connection) => {
        // Transformer l'appel à la fonction en Promise
        return new Promise<void>((resolve, reject) => {
            connection.close((err) => {
                if(err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    });
    await Promise.all(promises); // Attente de la fermeture de toutes les connections
}