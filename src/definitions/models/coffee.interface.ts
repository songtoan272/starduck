import {Id} from "./id.interface";

export interface ICoffee {
    id?: Id; // ? permet de pouvoir stocker undefined
    name: string;
    intensity: number;
    price: number;
}