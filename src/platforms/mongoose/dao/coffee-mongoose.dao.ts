import {BaseMongooseDAO} from "./base-mongoose.dao";
import {CoffeeModel, ICoffeeDocument} from "../models/coffee.model";
import {Model, Mongoose} from "mongoose";
import {ICoffeeDAO} from "../../../definitions/dao";
import {ICoffee} from "../../../definitions/models";
import {inject, injectable} from "inversify";

@injectable()
export class CoffeeMongooseDAO extends BaseMongooseDAO<ICoffeeDocument> implements ICoffeeDAO {

    public constructor(@inject(Mongoose) mongoose: Mongoose) {
        super(mongoose);
    }

    getModel(): Model<ICoffeeDocument> {
        return CoffeeModel;
    }

    save(coffee: ICoffee): Promise<ICoffeeDocument> {
        return this.getModel().create(coffee);
    }

    searchByName(name: string): Promise<ICoffeeDocument[]> {
        return this.getModel().find({
            name: {
                $regex: new RegExp(name),
                $options: "i"
            }
        }).exec();
    }
}