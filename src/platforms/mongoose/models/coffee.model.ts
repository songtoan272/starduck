import {ICoffee} from "../../../definitions/models";
import {Document, Schema} from "mongoose";
import * as mongoose from "mongoose";

export type ICoffeeDocument = ICoffee & Document;

export const CoffeeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    intensity: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    }
}, {
    versionKey: false,
    timestamps: true
});

export const CoffeeModel = mongoose.model<ICoffeeDocument>('Coffee', CoffeeSchema);