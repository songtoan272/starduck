import {ContainerModule, interfaces} from "inversify";
import * as http from "http";
import * as Koa from "koa";
import {SHARED_TYPES} from "../../ioc/types";
import {CoffeeController, StoreController} from "./controllers";
import {APP_TYPES} from "./ioc/types";
import * as bodyParser from "koa-bodyparser";
import {Connection} from "../../definitions";

export const appModule = new ContainerModule((bind: interfaces.Bind) => {
   bindRootController(bind, APP_TYPES.Controllers.Coffee, CoffeeController);
   bindRootController(bind, APP_TYPES.Controllers.Store, StoreController);
   bind<http.Server>(SHARED_TYPES.Server).toDynamicValue((context: interfaces.Context) => {
       const app = new Koa();
       app.use(bodyParser()); // Permet de transformer le body des requetes entrante
       const rootControllers: any[] = context.container.getAll(APP_TYPES.RootController);
       for(const controller of rootControllers) {
           const router = controller.build();
           app.use(router.routes()); // permet d'ajouter les routes du router dans l'application
           app.use(router.allowedMethods()); // permet d'acceder aux methodes GET, POST, PUT, ...
       }
       const port = process.env.PORT || 3000;
       const server = app.listen(port, () => console.log(`API Listening on ${port}...`));
       bind<Connection>(SHARED_TYPES.Connection).toConstantValue(server);
       return server;
   }).inSingletonScope();
});

/**
 * Permet d'ajouter dans le conteneur le controller en question ainsi que de l'ajouter aux rootcontrollers
 * @param bind Methode permettant d'ajouter des elements dans l'injecteur de dépendance
 * @param symbol Clé associée au controller
 * @param controller Le constructeur du controller (nom de classe)
 */
function bindRootController(bind: interfaces.Bind, symbol: symbol, controller: any): void {
    bind(symbol).to(controller).inSingletonScope();
    bind(APP_TYPES.RootController).toDynamicValue((context: interfaces.Context) => {
        return context.container.get(symbol);
    });
}