export const APP_TYPES = {
    RootController: Symbol.for('RootController'),
    Controllers: {
        Coffee: Symbol.for('CoffeeController'),
        Store: Symbol.for('StoreController')
    }
};