import * as Router from "koa-router";
import {Context} from "koa";
import {inject, injectable} from "inversify";
import {SHARED_TYPES} from "../../../ioc/types";
import {ICoffeeDAO} from "../../../definitions/dao";
import {plainToClass} from "class-transformer";
import {CoffeeDTO} from "../../../definitions/dto";
import {validate} from "class-validator";

@injectable()
export class CoffeeController {

    readonly coffeeDAO: ICoffeeDAO;

    constructor(@inject(SHARED_TYPES.DAO.Coffee) coffeeDAO: ICoffeeDAO) {
        this.coffeeDAO = coffeeDAO;
    }

    // Context -> Context KOA (request http, response, etc..)
    async getAll(ctx: Context) {
        if (ctx.query.name) {
            ctx.body = await this.coffeeDAO.searchByName(ctx.query.name);
            return;
        }
        ctx.body = await this.coffeeDAO.getAll();
    }

    async create(ctx: Context) {
        const data = ctx.request.body;
        const coffee = plainToClass(CoffeeDTO, data);
        const errors = await validate(coffee);
        if(errors.length > 0) {
            ctx.throw(400); // Permet de finir la requete et de retourner un status HTTP 400 (Bad request)
        }
        ctx.body = await this.coffeeDAO.save(coffee);
    }

    async delete(ctx: Context) {
        const id = ctx.params.id;
        try {
            await this.coffeeDAO.removeById(id);
            ctx.body = 'Deleted';
        } catch (err) {
            ctx.throw(err);
        }
    }

    build(): Router {
        const router = new Router({
            prefix: '/coffee'
        });
        router.get('/', this.getAll.bind(this))
        router.post('/', this.create.bind(this));
        router.delete('/:id', this.delete.bind(this));
        return router;
    }
}