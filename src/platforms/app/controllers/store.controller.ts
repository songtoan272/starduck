import * as Router from "koa-router";
import {Context} from "koa";
import {injectable} from "inversify";

@injectable()
export class StoreController {

    constructor() {
    }

    // Context -> Context KOA (request http, response, etc..)
    world(ctx: Context) {
        ctx.body = 'world store';
    }

    build(): Router {
        const router = new Router({
            prefix: '/store'
        });
        router.get('/hello', this.world.bind(this))
        return router;
    }
}