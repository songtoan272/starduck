import {Coffee} from "./coffee.class";
import {ICoffee} from "./definitions/models/coffee.interface";

const c = new Coffee({
    price: 2,
    name: 'Columbia',
    intensity: 7
});

const ic: ICoffee = {
    intensity: 15,
    price: 3,
    name: 'Intenso'
};
const c2 = new Coffee(ic);

console.log(c);
console.log(c2);