import 'reflect-metadata' // FORCE TO LOAD annotations
import * as dotenv from 'dotenv';
dotenv.config({
    path: '.test.env'
});